# MOSIP Config

## Introduction

MOSIP is Modular Open Source Identity Platform, read and learn more about it in the extensive [documentation](https://github.com/mosip-open/Documentation). The ***seed contribution to MOSIP is still work in progress***, consider this repo as an early preview for now. Today, we aim to give the community a sense of early direction with this preview. 

The issue list is open, but we will not act upon the issues till a formal release is behind us.

## Modularity & Config

MOSIP config is central location and suggested configuration for all MOSIP subsystems. Much of the modular aspect is driven by configuration.

Use this document to gain understanding of [configuration and launcher](https://github.com/mosip-open/Documentation/wiki/MOSIP-configuration-&-launcher).

### Masked properties

Many properties have <placeholder> patterns in their values. After checkout, insert proper values in consultation with the Getting Started Guide.